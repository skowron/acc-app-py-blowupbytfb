#!/acc/local/share/python/acc-py/pro/bin/python-env

import matplotlib.pyplot as plt
import pandas as pd
import pyjapc
import numpy as np



def readTFBgainFunc(japc: pyjapc._japc.PyJapc,  devname: str) -> list:
    ''' Returns current function in field Settings/amplitude
    It is list of single element, which is a list of 2 lists (not me who invented it!)
    returned variable is called amps
    values = amps[0][0]
    times = amps[0][1]

    '''

    import  time

    n = 0
    ntot = 0

    ap = None
    ad = None

    global fig1
    global axs1


    func = japc.getParam(f"{devname}/Setting")
    print(func)
    amps = func["amplitudes"]

    times = amps[0][0]
    values = amps[0][1]

    axs1[0].plot(times, values)
    plt.draw()
    plt.pause(0.05)


    plt.ion()
    plt.draw()
    plt.pause(0.05)
    plt.show()


    return amps

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--plot", action="store_true", default="true")
    parser.add_argument("-s", "--start", type=float, default=300)
    parser.add_argument("-e", "--end", type=float, default=310)
    parser.add_argument("-a1", "--amplitude1", type=float, default=-10)
    parser.add_argument("-a2", "--amplitude2", type=float, default=-10)
    parser.add_argument("-l", "--length", type=float, default=0.1)
    parser.add_argument("-x", "--lengthramp", type=float, default=0)
    parser.add_argument("-d", "--period", type=float, default=1)
    parser.add_argument("-c", "--cycle", default="PSB.USER.MD5")
    parser.add_argument("-r", "--ring", default=4, type=int)
    parser.add_argument("-p", "--plane", default="V")

    args = parser.parse_args()

    print("Passed settings:")
    print("  a1 ", args.amplitude1)
    print("  a2 ", args.amplitude2)
    print("  start ", args.start)
    print("  end ", args.end)
    print("  length ", args.length)
    print("  lengthramp ", args.lengthramp)
    print("  period ", args.period)

    import logging
    logging_format = "{asctime} {name:<25s} {message}"
    logging.basicConfig(format=logging_format, level=logging.INFO, style="{")


    m = f"Reading current function "
    logging.info(m)


    j = pyjapc.PyJapc(selector=args.cycle, noSet=False)
    print(type(j))

    devname = "BA" + str(args.ring) + ".GSTFBGAIN" + str(args.plane)
    print(devname)
    global fig1
    global axs1

    fig1, axs1 = plt.subplots(nrows=2, ncols=1, sharex='col', num='Old function')

    cfcn = readTFBgainFunc(j,devname)
    if cfcn is None:
        print("none")
        return



    newvalues = [0, 0, 0]
    newtimes = [0, 270, 275]

    starttime = args.start
    endtime = args.end

    mingap = 10

    currentendtime = starttime
    currentlength = args.length
    while (currentendtime + args.period) < endtime:
        ts = []
        vs = []

        currentamplitude = args.amplitude1 + (args.amplitude2-args.amplitude1)*(currentendtime - starttime)/(endtime - starttime)



        # start with zero
        localtime = 0
        ts.append(currentendtime + localtime)
        vs.append(0.0)

        # ramp up to required amplitude within 1 us
        localtime = localtime + 0.005
        ts.append(currentendtime + localtime)
        vs.append(currentamplitude * 20e-7*currentendtime*currentendtime)

        # keep constant amplitude for required length
        localtime = localtime + currentlength
        currentlength = currentlength + args.lengthramp

        ts.append(currentendtime + localtime)
        vs.append(currentamplitude * 20e-7*currentendtime*currentendtime )

        # ramp down back to zeroy
        localtime = localtime + 0.005
        ts.append(currentendtime + localtime)
        vs.append(0.0)

        currentendtime =  currentendtime + args.period
        if ts[-1] > currentendtime - mingap:
            currentendtime = ts[-1] +  mingap

        newvalues = newvalues + vs
        newtimes = newtimes + ts

    newvalues.append(0.0)
    newtimes.append(806)
    print(newvalues)
    print(newtimes)

    axs1[1].plot(newtimes, newvalues)


    plt.ioff()
    plt.draw()
    plt.pause(0.05)
    plt.show()

    g = input("Do you want to send it ?: ")
    if g != "y":
        print("Typed value different from y, not sending")
        return

    print("Sending new function")
    #    times = amps[0][0]
    #    values = amps[0][1]

    s = [newtimes, newvalues]
    ss = [s]
    toset = {}

    toset["amplitudes"] = ss

    retval = j.setParam(f"{devname}/Setting",toset)

    print(retval)
    return

    property_data = {'enabled': True,
                     'delay': args.start,
                     'length': samples_to_us(len(amps_full_buffer)),
                     'amplitude': amps_full_buffer,
                     'phase': phas_full_buffer
                     }

    if g == "1":

        kldevice = "L4P.ACAVLOOP.PIMS1112"
        logging.info(f"Writing to the device... {kldevice}")
        j.setParam(f"{kldevice}/VoltageSetPointFunction", property_data, checkDims=False)
    else:
        logging.info(f"Exiting without sending anything")


if __name__ == "__main__":
    main()

